require 'redis'
require 'net/http'
require 'net/https'

class RdCache
  class << self
    def client
      @@client
    end

    def initialize
      @@config ||= ConfigService.load_config('redis.yml')[ConfigService.environment]
      @@client ||= Redis.new(url: "redis://#{@@config['host']}")
      cache_exists?
    rescue Exception => error
      puts("RdCache.initialize error: #{error.message}")
      @@client = nil
    end #initialize

    def cache_exists?
      Net::HTTP.get(URI("http://#{@@config['host']}"))
    rescue Errno::ECONNREFUSED => error
      puts "**** Error: #{error.message}"
      @@client = nil
    rescue Net::HTTPBadResponse => error
      # do nothing
    end

    # Cache API, mimics ActiveSupport::Cache::Store
    # http://api.rubyonrails.org/classes/ActiveSupport/Cache/Store.html
    def read(key, options = {})
      return unless client
      client.get(key)
    end

    def write(key, value, options = {})
      return unless client
      client.set(key, value)
      client.expire(key, options[:expires_in]) if  options[:expires_in]
    end

    def delete(key, options = {})
      return unless client
      deleted = read(key)
      client.del(key)
      deleted
    end
  end #class methods

  initialize
end
