require_relative('../rspec_helper')

describe RdCache do
  before :all do
    RdCache.initialize
    raise 'You must have redis installed somewhere to run the tests' unless RdCache.client
  end

  before :each do
    RdCache.initialize
    @client = RdCache.client
  end

  # This test initialize and the private init_zookeeper too
  context 'initialize' do
    it 'initializes the memcached client' do
      RdCache.initialize
      expect(RdCache.client).not_to be_nil
    end

    it 'initializes the memcached client to nil if error happens' do
      allow(RdCache).to receive(:cache_exists?).and_raise('Something wrong')
      RdCache.initialize
      expect(RdCache.client).to be_nil
    end
  end

  context 'write' do
    it 'writes data to memcached' do
      RdCache.write('test_key', 'Some data')
      expect(@client.get('test_key')).to eql('Some data')
    end

    it 'returns nil if RdCache.client is nil' do
      expect(RdCache).to receive(:client).and_return(nil)
      expect(RdCache.write('test_key', 'Some data')).to be_nil
    end
  end

  context 'read' do
    it 'reads data from memcached' do
      @client.set('test_key', 'Some other data')
      expect(RdCache.read('test_key')).to eql('Some other data')
    end

    it 'returns nil if the key does not exist' do
      expect(RdCache.read('test_key')).to be_nil
    end

    it 'returns nil if the data is expired' do
      RdCache.write('test_key', 'Some data', { expires_in: 1 })
      sleep 2
      expect(RdCache.read('test_key')).to be_nil
    end

    it 'returns nil if RdCache.client is nil' do
      expect(RdCache).to receive(:client).and_return(nil)
      expect(RdCache.read('test_key')).to be_nil
    end
  end

  context 'delete' do
    it 'deletes the object and returns it if the key exists' do
      RdCache.write('other_test_key', 'Some data')
      deleted = RdCache.delete('other_test_key')
      expect(RdCache.read('other_test_key')).to be_nil
      expect(deleted).to eql('Some data')
    end

    it 'returns nil if the key does not exist' do
      deleted = RdCache.delete('does_not_exist_test_key')
      expect(deleted).to eql(nil)
    end

    it 'returns nil if RdCache.client is nil' do
      expect(RdCache).to receive(:client).and_return(nil)
      expect(RdCache.delete('test_key')).to be_nil
    end
  end

  after :each do
    @client.del('test_key') if @client
  end
end
